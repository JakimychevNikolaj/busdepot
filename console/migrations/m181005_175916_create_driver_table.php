<?php

use yii\db\Migration;

/**
 * Handles the creation of table `driver`.
 */
class m181005_175916_create_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('driver', [
			'driverId' => $this->primaryKey(),
			'photo' => $this->string(),
			'fullName' => $this->string()->notNull(),
			'birthDate' => $this->bigInteger(),
			'active' => $this->boolean()->defaultValue(true),
			'busId' => $this->integer()
		]);
		$this->addForeignKey(
			'fk-driver-bus-1',
			'driver',
			'busId',
			'bus',
			'busId',
			'SET NULL'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('fk-driver-bus-1', 'driver');
        $this->dropTable('driver');
    }
}
