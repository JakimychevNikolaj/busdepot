<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bus`.
 */
class m181005_155940_create_bus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bus', [
            'busId' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'avgSpeed' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bus');
    }
}
