<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m181005_174018_add_admin2
 */
class m181005_174018_add_admin2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
	{
		$email = 'admin2@mail.com';

		$user = new User();
		$user->username = stristr($email, '@', true);
		$user->email = $email;
		$user->role = User::ROLE_ADMIN2;
        $user->setPassword('admin2');
        $user->generateAuthKey();
		return $user->save();

	}
}
