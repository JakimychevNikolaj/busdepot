<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m181005_164659_add_admin1
 */
class m181005_164659_add_admin1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
	{
		$email = 'admin@mail.com';

		$user = new User();
		$user->username = stristr($email, '@', true);
		$user->email = $email;
		$user->role = User::ROLE_ADMIN1;
        $user->setPassword('admin');
        $user->generateAuthKey();
		return $user->save();

	}

}
