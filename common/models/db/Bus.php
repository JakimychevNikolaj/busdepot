<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "bus".
 *
 * @property int $busId
 * @property string $name
 * @property int $avgSpeed
 *
 * @property Driver[] $drivers
 */
class Bus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['avgSpeed'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'busId' => 'Bus ID',
            'name' => 'Name',
            'avgSpeed' => 'Avg Speed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['busId' => 'busId']);
    }
}
