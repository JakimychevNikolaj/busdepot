<?php

namespace common\models\db;

use Yii;
use DateTime;

/**
 * This is the model class for table "driver".
 *
 * @property int $driverId
 * @property string $photo
 * @property string $fullName
 * @property int $birthDate
 * @property int $active
 * @property int $busId
 *
 * @property Bus $bus
 */
class Driver extends \yii\db\ActiveRecord
{
	public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * {@inheritdoc}
     */
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if (!empty($this->birthDate) && !is_numeric($this->birthDate)) {
				$this->birthDate = strtotime($this->birthDate);
			}
			return true;
		}
		return false;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullName'], 'required'],
            [['active', 'busId'], 'integer'],
            [['birthDate'], 'safe'],
            [['photo', 'fullName'], 'string', 'max' => 255],
            [['busId'], 'exist', 'skipOnError' => true, 'targetClass' => Bus::className(), 'targetAttribute' => ['busId' => 'busId']],
			[['file'], 'file', 'extensions' => 'png, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'driverId' => 'ID',
            'photo' => 'Photo',
            'fullName' => 'Full Name',
            'birthDate' => 'Birth Date',
            'active' => 'Active',
            'busId' => 'Bus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBus()
    {
        return $this->hasOne(Bus::className(), ['busId' => 'busId']);
    }


    /**
     * @return string
     */
	public function getAge()
	{
		$date = new DateTime();
		$date->setTimestamp($this->birthDate);
		$now = new DateTime();
		
		return $now->diff($date)->y;
	}
}
