<?php

namespace frontend\controllers;

use Yii;
use common\models\db\Driver;
use frontend\models\DriverSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use frontend\controllers\PrivateController;
use common\models\User;
use yii\web\UploadedFile;

use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;

/**
 * DriverController implements the CRUD actions for Driver model.
 */
class DriverController extends PrivateController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
		$behaviors = parent::behaviors();

		$behaviors['verbs'] = [
			'class' => VerbFilter::className(),
			'actions' => [
				'delete' => ['POST'],
				'change-status' => ['POST'],
			]
		];

		return $behaviors;
    }

    /**
     * Lists all Driver models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriverSearch();
		$searchModel->load(Yii::$app->request->post());
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination->pageSize=3;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Driver model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Driver model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$user = User::getUser();
		if (empty($user) || (!$user->isAdmin1() && !$user->isAdmin2())) {
			Yii::$app->session->setFlash('error', "You do not have enough permissions.");
			return $this->redirect(['index']);
		}
        $model = new Driver();

        if ($model->load(Yii::$app->request->post())) {
			$model->file = UploadedFile::getInstance($model, 'file');
			if ($model->file && $model->validate() &&
				$model->file->saveAs('uploads/' . $model->file->baseName . '.' . $model->file->extension)
			) {                
				$fileName = 'uploads/' . $model->file->baseName . '.' . $model->file->extension;
				$savePath = Yii::getAlias('@frontend/web/uploads') . '/';
				$fullPath = $savePath . $model->file->baseName . '.' . $model->file->extension;
				Image::getImagine()->open($fileName)->thumbnail(new Box(500, 300))->save($fullPath , ['quality' => 90]);
				$model->photo = $fullPath;
				$model->file = null;
			}
			$model->save();
			return $this->redirect(['view', 'id' => $model->driverId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

	/**
     * @param integer $id
     * @param integer $status
	 */
	public function actionChangeStatus() 
	{
		$id = $_POST['id'];
		$status = $_POST['status'];
		$page = $_POST['page'];

        $model = $this->findModel($id);
		if ($model) {
			$model->active = $status;
			$model->save();
		} 
		if (Yii::$app->getRequest()->isAjax) {
			$searchModel = new DriverSearch();
			$dataProvider = new ActiveDataProvider([
				'query' => Driver::find()->orderBy(['fullName' => SORT_ASC]),
				'sort' => false
			]);
			$dataProvider->pagination->pageSize=3;
			$dataProvider->pagination->route='driver/index';
			$dataProvider->pagination->page = $page - 1;
			return $this->renderPartial('index', [
				'dataProvider' => $dataProvider,
				'searchModel' => $searchModel
			]);
		}
		return $this->redirect(['index']);
	}

    /**
     * Updates an existing Driver model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
		$user = User::getUser();
		if (empty($user) || (!$user->isAdmin1() && !$user->isAdmin2())) {
			Yii::$app->session->setFlash('error', "You do not have enough permissions.");
			return $this->redirect(['index']);
		}
        $model = $this->findModel($id);
		$model->birthDate = ($model->birthDate) ? date('Y-m-d', $model->birthDate) : $model->birthDate;

        if ($model->load(Yii::$app->request->post())) {
			$model->file = UploadedFile::getInstance($model, 'file');
			if ($model->file && $model->validate() &&
				$model->file->saveAs('uploads/' . $model->file->baseName . '.' . $model->file->extension)
			) {                
				$fileName = 'uploads/' . $model->file->baseName . '.' . $model->file->extension;
				$savePath = Yii::getAlias('@frontend/web/uploads') . '/';
				$fullPath = $savePath . $model->file->baseName . '.' . $model->file->extension;
				Image::getImagine()->open($fileName)->thumbnail(new Box(500, 300))->save($fullPath , ['quality' => 90]);
				$model->photo = $fullPath;
				$model->file = null;
			}
			$model->save();
			return $this->redirect(['view', 'id' => $model->driverId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Driver model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		$user = User::getUser();
		if (empty($user) || !$user->isAdmin2()) {
			Yii::$app->session->setFlash('error', "You do not have enough permissions.");
			return $this->redirect(['index']);
		}
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Driver model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Driver the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driver::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
