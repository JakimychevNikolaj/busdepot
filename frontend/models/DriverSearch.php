<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\db\Driver;

/**
 * DriverSearch represents the model behind the search form of `common\models\db\Driver`.
 */
class DriverSearch extends Driver
{
	public $fromCity;
	public $toCity;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['driverId', 'active', 'busId'], 'integer'],
            [['photo', 'fullName', 'birthDate', 'fromCity', 'toCity'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Driver::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'driverId' => $this->driverId,
            'active' => $this->active,
            'busId' => $this->busId,
        ]);

        $query->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'fullName', $this->fullName])
            ->andFilterWhere(['like', 'birthDate', $this->birthDate]);

		$query->orderBy(['fullName' => SORT_ASC]);

        return $dataProvider;
    }
}
