<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\db\Bus;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\db\Driver */

$this->title = $model->fullName;
$this->params['breadcrumbs'][] = ['label' => 'Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->driverId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->driverId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'driverId',
			[
				'attribute' => 'photo',
				'format' => 'html',
				'value' => function ($model) {
					if (empty($model->photo)) {
						return '';
					}
					$url = Url::to($model->photo, true);
					return "<img src=\"$url\" alt='img'>";
				}
			],
            'fullName',
			[
				'attribute' => 'birthDate',
				'label' => 'Age',
				'format' => 'raw',
				'value' => function ($model) {
					if (empty($model->birthDate)) {
						return '';
					}
					return $model->getAge();
				}
			],
			[
				'attribute' => 'active',
				'format' => 'raw',
				'value' => function ($model) {
					return ($model->active) ? 'Yes' : 'No';
				}
			],
			[
				'attribute' => 'busId',
				'format' => 'raw',
				'value' => function ($model) {
					$bus = Bus::findOne($model->busId);
					return ($bus) ? $bus->name : '';
				}
			],
        ],
    ]) ?>

</div>
