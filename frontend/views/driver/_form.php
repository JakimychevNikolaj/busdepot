<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\db\Bus;

/* @var $this yii\web\View */
/* @var $model common\models\db\Driver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'file')->fileInput()->label('Photo') ?>

    <?= $form->field($model, 'fullName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthDate')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => 'Enter birth date ...'],
		'pluginOptions' => [
			'autoclose' => true,
			'format' => 'yyyy-mm-dd'
		]
	]); ?>
	<?= $form->field($model, 'active')->dropDownList([0 => 'No', 1 => 'Yes']) ?>

	<?= $form->field($model, 'busId')->dropDownList(ArrayHelper::map(Bus::find()->all(), 'busId', 'name')) ?>

	<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
