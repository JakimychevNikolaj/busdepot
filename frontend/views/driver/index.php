<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\db\Bus;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DriverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Drivers';
$this->params['breadcrumbs'][] = $this->title;
$currentPage = isset($_GET['page']) ? $_GET['page'] : 0;


$actionColumn = ['class' => 'yii\grid\ActionColumn',
		'template' =>'{view} {update} {delete} {active} {inactive}',
		'buttons' => [
			'active' => function ($url, $model) use ($currentPage) {
				return Html::a('<span class="btn btn-success">Active</span>',
					['driver/change-status'],
					[
						'data-pjax'=>'w1',
						'data-method' => 'POST',
						'data-params' => [
							'id' => $model->driverId,
							'status' => 1,
							'page' => $currentPage,

						],
					]
				);
			},
			'inactive' => function ($url, $model) use ($currentPage) {
				return Html::a('<span class="btn btn-warning">Inactive</span>',
					['driver/change-status'],
					[
						'data-pjax'=>'w1',
						'data-method' => 'POST',
						'data-params' => [
							'id' => $model->driverId,
							'status' => 0,
							'page' => $currentPage,
						],
					]
				);
			},
		],
		'visibleButtons' => [
			'active' => function ($model, $key, $index) {
				return !$model->active;
			},
			'inactive' => function ($model, $key, $index) {
				return $model->active;
			},
		]
	];


/**
 * @return array
 */
function getCoordinates($city)
{
	$address = $city;
	$url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=Russia&key=";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$response = curl_exec($ch);
	curl_close($ch);
	$response_a = json_decode($response);
	$status = $response_a->status;

	if ( $status == 'ZERO_RESULTS' ) {
		return FALSE;
	} else {
		//need google api key
		return array('lat' => $response_a->results[0]->geometry->location->lat, 'long' => $long = $response_a->results[0]->geometry->location->lng);
	}
}

/**
 * @return string
 */
function obtainDistance($lat1, $lat2, $long1, $long2)
{
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL&key=";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    return $dist;
}

$distance = null;
if (!empty($searchModel->fromCity) && !empty($searchModel->toCity)) {
	$coordinates1 = getCoordinates($searchModel->fromCity);
	$coordinates2 = getCoordinates($searchModel->toCity);
	if ($coordinates1 && $coordinates2) {
		$distance = obtainDistance($coordinates1['lat'], $coordinates2['lat'], $coordinates1['long'], $coordinates2['long']);
	}
}

?>
<div class="driver-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Driver', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php $form = ActiveForm::begin(['action' => ['index']]); ?>

		<div class="row">
			<div class="col-md-6">
				<?= $form->field($searchModel, 'fromCity')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-6">
				<?= $form->field($searchModel, 'toCity')->textInput(['maxlength' => true]) ?>
			</div>
		</div>

		<div class="form-group">
			<?= Html::submitButton('Calculate', ['class' => 'btn btn-success']) ?>
		</div>
	
	<?php ActiveForm::end(); ?>

	<?php yii\widgets\Pjax::begin(['id' => 'demo']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            'driverId',
			[
				'attribute' => 'photo',
				'format' => 'html',
				'value' => function ($model) {
					if (empty($model->photo)) {
						return '';
					}
					$url = Url::to($model->photo, true);
					return "<img src=\"$url\" alt='img'>";
				}
			],
            'fullName',
			[
				'attribute' => 'birthDate',
				'label' => 'Age',
				'format' => 'raw',
				'value' => function ($model) {
					if (empty($model->birthDate)) {
						return '';
					}
					return $model->getAge();
				}
			],
			[
				'attribute' => 'active',
				'format' => 'raw',
				'value' => function ($model) {
					return ($model->active) ? 'Yes' : 'No';
				}
			],
			[
				'attribute' => 'busId',
				'format' => 'raw',
				'value' => function ($model) {
					$bus = Bus::findOne($model->busId);
					return ($bus) ? $bus->name : '';
				}
			],
			[
				'label' => 'Travel Time',
				'format' => 'raw',
				'visible' => !empty($distance),
				'value' => function ($model) use ($distance) {
					$bus = Bus::findOne($model->busId);
					if (!$bus || !$bus->avgSpeed) {
						return '';
					}
					$avgDayDistance = $bus->avgSpeed * 8; 
					return round($distance / $avgDayDistance, 2) . ' day(s)';
				}
			],

			$actionColumn
        ],
    ]); ?>
	<?php yii\widgets\Pjax::end(); ?>
</div>
